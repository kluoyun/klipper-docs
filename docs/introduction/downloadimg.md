# 系统镜像下载

> 如果是重新烧录系统，请提前备份好配置文件。

?> 以后只提供最新版本的下载链接

* v2.9.4  [2022-06-13](https://upyun.pan.zxkxz.cn/IMG/Build/FLY-v2.9.4_Flygemini_bullseye_current_5.10.85.img.xz)

?> (A)版本镜像将不会预装klipper等环境，为纯净版Armbian

* v2.9.4(A)  [2022-06-22](https://upyun.pan.zxkxz.cn/IMG/Build/FLY-v2.9.4(A)_Flygemini_bullseye_current_5.10.85.img.xz)